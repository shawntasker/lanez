﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    public int health = 100;
    public int XP = 10;
    public float knockbackSpeed = 3f;
	public float speed = 0f;

    private Rigidbody2D rb2d;
    private float force = 300f;
    private Vector3 lastPosition;
    private Vector3 start;
    private Transform trans;
    private Player player;
	private Transform playerTransform;
	private bool awake;
    private float moveSpeed;
    private Vector3 delta;
    private Vector3 vectorToTarget;
    private float moveDistance;

    // Use this for initialization
    void Start () {
        rb2d = GetComponent<Rigidbody2D>();
        health = 100;
        player = FindObjectOfType<Player>();
		playerTransform = player.transform;
	}
	
	// Update is called once per frame
	void Update () {
		MoveTowardPlayer ();
	}

    void WakeUp()
    {
        awake = true;
    }

    void MoveTowardPlayer()
    {
        if(awake)
        {
            vectorToTarget = player.transform.position - transform.position;
            if(vectorToTarget.magnitude > 1 && vectorToTarget.magnitude < 6)
            {
                moveDistance = speed * Time.deltaTime;
                transform.Translate(vectorToTarget.normalized * speed);
                transform.position += vectorToTarget.normalized * moveDistance;
                vectorToTarget = player.transform.position - transform.position;
            }
            else
            {
                awake = false;
            }
        }
        // old method
        //delta = player.transform.position - transform.position;
        //delta.Normalize();

        //moveSpeed = speed * Time.deltaTime;

        //transform.position = transform.position + (delta * moveSpeed);
    }

    public void Knockback() // TODO: Implement this
    {
        trans = GetComponent<Transform>();
        Vector3 moveDir = trans.position - player.transform.position;
        trans.Translate(moveDir.normalized * knockbackSpeed * Time.deltaTime);
    }

    public void Hit()
    {
		health = health - 10;
        if(health <= 0)
        {
            Destroy(gameObject);
            return;
            // player.addXP(XP);
			// maybe put all xp / score in at static class 
			// call the class Scorekeeper?
        }
        return;
    }
    
}
