﻿using UnityEngine;
using System.Collections;

public class CollisionTest : MonoBehaviour {

	void OnCollision2D(Collider2D collider)
    {
        Debug.Log("Collided with " + collider.gameObject);
    }
}
