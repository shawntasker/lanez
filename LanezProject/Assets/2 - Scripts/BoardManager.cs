﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour {

    [Serializable]
    public class Count
    {
        public int minimum;
        public int maximum;

        public Count (int min, int max)
        {
            minimum = min;
            maximum = max;
        }
    }

    public int columns = 100;
    public int rows = 100;
    public Count wallCount = new Count(10, 18);
    public Count foodCount = new Count(2, 10);
    public GameObject exit;
    public GameObject[] floorTiles;
    public GameObject[] wallTiles;
    public GameObject[] foodTiles;
    public GameObject[] enemyTiles;
    public GameObject[] outerWallTiles;

    private Transform boardHolder; // used to child everything to in order to tidy the heirarchy
    private List<Vector3> gridPositions = new List<Vector3>();

    void InitialiseList()
    {
        gridPositions.Clear();

        for(int x = 1; x < columns - 1; x++)
        {
            for(int y = 1; y < rows - 1; y++)
            {
                gridPositions.Add(new Vector3(x, y, 0f));
            }
        }
    }

    void BoardSetup()
    {
        boardHolder = new GameObject("Board").transform;

        for(int x = -1; x < columns + 1; x++)
        {
            for(int y = -1; y < rows + 1; y++)
            {
                GameObject toInstantiate = floorTiles[Random.Range(0, floorTiles.Length)];
                if(x == -1 || x == columns || y == -1 || y == rows)
                {
                    toInstantiate = outerWallTiles[Random.Range(0, outerWallTiles.Length)];
                }
                GameObject instance = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;

                instance.transform.SetParent(boardHolder);
            }
        }
    }

    Vector3 RandomPosition()
    {
        int randomIndex = Random.Range(0, gridPositions.Count);
        Vector3 randomPosition = gridPositions[randomIndex];
        gridPositions.RemoveAt(randomIndex);

        return randomPosition;
    }

    void LayoutObjectAtRandom(GameObject[] tileArray, int minimum, int maximum)
    {
        int objectCount = Random.Range(minimum, maximum + 1); // stores how many of an object that will spawn

        for (int i = 0; i < objectCount; i++)
        {
            Vector3 randomPosition = RandomPosition();
            GameObject tileChoice = tileArray[Random.Range(0, tileArray.Length)];
            Instantiate(tileChoice, randomPosition, Quaternion.identity);
        }

    }

    public void SetupScene(int level)
    {
        
        BoardSetup();
        InitialiseList();
        ExitSetup();    
        // TODO: Call special exit setup mehod here

        LayoutObjectAtRandom(wallTiles, wallCount.minimum, wallCount.maximum);
        LayoutObjectAtRandom(foodTiles, foodCount.minimum, foodCount.maximum);
        int enemyCount = (rows * columns) / 50;
        LayoutObjectAtRandom(enemyTiles, enemyCount, enemyCount);
        // Instantiate(exit, new Vector3(columns - 1, rows - 1, 0f), Quaternion.identity);
    }

    void ExitSetup()
    {
        // put the exit at a random coordinate
        Vector3 randomExitPosition = RandomPosition();
        // if the exit is too close to the edge, find another coordinate
        while(randomExitPosition.x + 3 > columns || randomExitPosition.y > rows)
        {
            randomExitPosition = RandomPosition();
        }

        Instantiate(exit, randomExitPosition, Quaternion.identity);

        List<Vector3> exitTop = new List<Vector3>();
        List<Vector3> exitRight = new List<Vector3>();
        List<Vector3> exitLeft = new List<Vector3>();
        List<Vector3> exitBottom = new List<Vector3>();

        bool top = false;
        bool right = false;
        bool bottom = false;
        bool left = false;

        // Top Coordinates = 1

        exitTop.Add(new Vector3(randomExitPosition.x, randomExitPosition.y + 2, 0f));
        exitTop.Add(new Vector3(randomExitPosition.x - 1, randomExitPosition.y + 2, 0f));
        exitTop.Add(new Vector3(randomExitPosition.x + 1, randomExitPosition.y + 2, 0f));

        // Right Coords = 2

        exitRight.Add(new Vector3(randomExitPosition.x + 2, randomExitPosition.y, 0f));
        exitRight.Add(new Vector3(randomExitPosition.x + 2, randomExitPosition.y + 1, 0f));
        exitRight.Add(new Vector3(randomExitPosition.x + 2, randomExitPosition.y - 1, 0f));

        // Bottom = 3

        exitBottom.Add(new Vector3(randomExitPosition.x, randomExitPosition.y - 2, 0f));
        exitBottom.Add(new Vector3(randomExitPosition.x - 1, randomExitPosition.y - 2, 0f));
        exitBottom.Add(new Vector3(randomExitPosition.x + 1, randomExitPosition.y - 2, 0f));

        // Left Coords = 4

        exitLeft.Add(new Vector3(randomExitPosition.x - 2, randomExitPosition.y - 1, 0f));
        exitLeft.Add(new Vector3(randomExitPosition.x - 2, randomExitPosition.y, 0f));
        exitLeft.Add(new Vector3(randomExitPosition.x - 2, randomExitPosition.y + 1, 0f));

        

        // Choose which side the enemies spawn on
        // Top

        int enemySide = Random.Range(1, 4);

        // Instantiate enemies on that side

        if(enemySide == 1) // Top
        {
            foreach(Vector3 coord in exitTop)
            {
                Instantiate(enemyTiles[Random.Range(0, 1)], coord, Quaternion.identity);
            }
            top = true;
        }
        if (enemySide == 2) // Right
        {
            foreach (Vector3 coord in exitRight)
            {
                Instantiate(enemyTiles[Random.Range(0, 1)], coord, Quaternion.identity);
            }
            right = true;
        }
        if (enemySide == 3) // Bottom
        {
            foreach (Vector3 coord in exitBottom)
            {
                Instantiate(enemyTiles[Random.Range(0, 1)], coord, Quaternion.identity);
            }
            bottom = true;
        }
        if (enemySide == 4) // Left
        {
            foreach (Vector3 coord in exitLeft)
            {
                Instantiate(enemyTiles[Random.Range(0, 1)], coord, Quaternion.identity);
            }
            left = true;
        }

        // Instantiating wall tiles in the other sides

        if(!top)
        {
            foreach (Vector3 coord in exitTop)
            {
                Instantiate(wallTiles[Random.Range(0, 1)], coord, Quaternion.identity);
            }
        }
        if (!right)
        {
            foreach (Vector3 coord in exitRight)
            {
                Instantiate(wallTiles[Random.Range(0, 1)], coord, Quaternion.identity);
            }
        }
        if (!bottom)
        {
            foreach (Vector3 coord in exitBottom)
            {
                Instantiate(wallTiles[Random.Range(0, 1)], coord, Quaternion.identity);
            }
        }
        if (!left)
        {
            foreach (Vector3 coord in exitLeft)
            {
                Instantiate(wallTiles[Random.Range(0, 1)], coord, Quaternion.identity);
            }
        }
    }
}
