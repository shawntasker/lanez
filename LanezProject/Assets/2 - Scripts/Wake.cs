﻿using UnityEngine;
using System.Collections;

public class Wake : MonoBehaviour {
    
	void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Enemy")
        {
            other.SendMessage("WakeUp");
        }

    }
}
