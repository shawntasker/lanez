﻿using UnityEngine;
using System.Collections;

public class Sword : MonoBehaviour {

    public float thrust = 1f;

    private Animator enemyAnimator;
    private Enemy enemy;


    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Enemy")
        {
            enemyAnimator = other.GetComponent<Animator>();
            enemyAnimator.SetTrigger("EnemyAttackedTrigger");

            other.SendMessage("Knockback");

            other.transform.SendMessage("Hit");
        }
    }
}
