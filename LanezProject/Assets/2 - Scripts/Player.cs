﻿using UnityEngine;
using System.Collections;
using System;

public class Player : MonoBehaviour {

    public int wallDamage = 1;
    public int pointsPerFood = 10;
    public int pointsPerSoda = 20;
    public float restartLevelDelay = 1f;
    public float playerSpeed = 10f;
	public float awakeRadius = 5f;

    private Animator animator;
    private Sword sword;
    private Animator swordAnimator;
    private int food;
    private Rigidbody2D rb2d;
	private Vector3 center;
    private string lastFaceingState = "Right";

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        sword = FindObjectOfType<Sword>();
        swordAnimator = sword.GetComponent<Animator>();
		center = new Vector3 (transform.position.x, transform.position.y);

        food = GameManager.instance.playerFoodPoints;
	}

    private void OnDisable()
    {
        GameManager.instance.playerFoodPoints = food;
    }
	
	// Update is called once per frame
	void Update () { // TODO: need to look up a stardard method to move my player and enemies

        Move();
	}

    void Move()
    {
        if (Input.GetKeyDown("w") && !Input.GetKeyDown("d") && !Input.GetKeyDown("a")) // don't want to move sideways while looking up or down
        {
            animator.SetTrigger("Up");
            lastFaceingState = "Up";
        }
        if (Input.GetKeyDown("a"))
        {
            animator.SetTrigger("Left");
            lastFaceingState = "Left";
        }
        if (Input.GetKeyDown("s") && !Input.GetKeyDown("d") && !Input.GetKeyDown("a")) // don't want to move sideways while looking up or down
        {
            animator.SetTrigger("Down");
            lastFaceingState = "Down";
        }
        if (Input.GetKeyDown("d"))
        {
            animator.SetTrigger("Right");
            lastFaceingState = "Right";
        }
        if (Input.GetKey("w"))
        {
            transform.Translate(0, playerSpeed * Time.deltaTime, 0);
        }
        if (Input.GetKey("a"))
        {
            transform.Translate(-playerSpeed * Time.deltaTime, 0, 0);
        }
        if (Input.GetKey("s"))
        {
            transform.Translate(0, -playerSpeed * Time.deltaTime, 0);
        }
        if (Input.GetKey("d"))
        {
            transform.Translate(playerSpeed * Time.deltaTime, 0, 0);
        }
        if (Input.GetMouseButtonDown(0))
        {
            if(lastFaceingState.Equals("Up"))
            {
                swordAnimator.SetTrigger("SwingUp");
            }
            if (lastFaceingState.Equals("Right"))
            {
                swordAnimator.SetTrigger("SwingRight");
            }
            if (lastFaceingState.Equals("Down"))
            {
                swordAnimator.SetTrigger("SwingDown");
            }
            if (lastFaceingState.Equals("Left"))
            {
                swordAnimator.SetTrigger("SwingLeft");
            }
        }


        // TODO: set pivot of sword based on direction player is facing
        // going to nest the attack getmousebutton in with the other if statements
    }

    void OnCollision2D(Collision2D collision)
    {
        Debug.Log("Collided with p " + collision.gameObject);
    }
    

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Exit")
        {
            Invoke("Restart", restartLevelDelay);
            enabled = false;
        }
        else if(other.tag == "Food")
        {
            food += pointsPerFood;
            other.gameObject.SetActive(false);
        }
        else if (other.tag == "Soda")
        {
            food += pointsPerSoda;
            other.gameObject.SetActive(false);
        }
    }

    private void Restart()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    public void LoseFood(int loss) // like health
    {
        animator.SetTrigger("playerHit");
        food -= loss;
        CheckIfGameOver();
    }

    private void CheckIfGameOver()
    {
        if(food <= 0)
        {
            GameManager.instance.GameOver();
        }
    }
}
